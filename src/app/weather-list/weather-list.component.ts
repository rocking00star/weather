import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.css']
})
export class WeatherListComponent implements OnInit {

  weathers: any[]
  constructor(private weatherservice : WeatherService,
    private router: Router
    ){}


  fetchweather(){
    // this.weather$ = this.weatherservice.fetchweather();
    this.weatherservice.fetchweather().subscribe(response => {
      this.weathers = response.list
    })
  }

  ngOnInit() {
  }
  onweatherclick(weather) {
    // localStorage.setItem('weather', JSON.stringify(weather))
    this.weatherservice.setcurrentweather(weather)
    this.router.navigateByUrl('/detail');
  }
}
